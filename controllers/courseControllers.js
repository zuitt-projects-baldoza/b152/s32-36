//import course Model
const Course = require("../models/Course");

module.exports.registerCourse = (req,res) => {

console.log(req.body);

let newCourse = new Course ({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
	});

	newCourse.save()
	.then(course=>res.send(course))
	.catch(err => res.send(err));
	// res.send("Test route");
}

module.exports.getAllCourseController=(req,res)=>{
		Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.getSingleCourse=(req,res)=>{
	Course.findById(req.params.id)
	.then(result=> res.send(result))
	.catch(error=> res.send(error));
}

//update course
module.exports.updateCourse=(req,res)=>{
	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}
	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
}


module.exports.archiveCourse=(req,res)=>{
	let archive = {
		isActive:false
	}
	Course.findByIdAndUpdate(req.params.id,archive,{new:true})
	.then(archiveCourse => res.send(archiveCourse))
	.catch(err => res.send(err));
}

module.exports.activateCourse=(req,res)=>{
	let archive = {
		isActive:true
	}
	Course.findByIdAndUpdate(req.params.id,archive,{new:true})
	.then(archiveCourse => res.send(archiveCourse))
	.catch(err => res.send(err));
}

module.exports.getAllActiveCourse=(req,res)=>{
	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.getInactiveCourse=(req,res)=>{
	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


module.exports.findCoursesByName=(req,res)=>{
	console.log(req.body.name)

	Course.find({name: req.body.name})
	.then(result => {
	console.log(result);

	if(result.length === 0){
		return res.send("No course found.")
	} else {
		return res.send(result)
	}
})
	.catch(err=> res.send(err))

}

module.exports.findCoursesByPrice=(req,res)=>{
	console.log(req.body.price)

	Course.find({name: req.body.price})
	.then(result => {
	console.log(result);

	if(result.length === 0){
		return res.send("No  found.")
	} else {
		return res.send(result)
	}
})
	.catch(err=> res.send(err))

}

module.exports.getEnrollees=(req,res)=>{

	Course.findById(req.params.id)
	.then(course => res.send(course.enrollees))
	.catch(error => res.send(error))
};
