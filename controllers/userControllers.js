//import User Model
const User = require("../models/User");

//import Course Model
const Course = require("../models/Course");

//import bcrypt
const bcrypt = require("bcrypt");

const auth = require("../auth");
// console.log(auth);

module.exports.registerUser = (req,res) => {

console.log(req.body);

const hashedPW = bcrypt.hashSync(req.body.password,10);
// console.log(hashedPW);
let newUser = new User ({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	});

	newUser.save()
	.then(user=>res.send(user))
	.catch(err => res.send(err));
	// res.send("Test route");
}

module.exports.getAllUsersController=(req,res)=>{
		User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.loginUser = (req,res) => {
	//Does this controller need user input?
	//yes
	//Where can we get this user input?
	//req.body
	console.log(req.body);

User.findOne({email: req.body.email})
.then(foundUser => {
	console.log(foundUser);

	if(foundUser === null){
		return res.send("No User Found.")
	} else {
		const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
		console.log(isPasswordCorrect);
	
	if(isPasswordCorrect){
		/*
		*/
		return res.send({accessToken: auth.createAccessToken(foundUser)})
	} else {
		return res.send("Incorrect Password.")
		}
	}
})
.catch(err=>res.send(err));

}


module.exports.getUserDetails=(req,res)=>{

	console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
	//res.send("testing for verify");
}


module.exports.checkEmailExists =(req,res)=>{
	console.log(req.body.email)
	
	User.findOne({email: req.body.email})
	.then(result => {
	console.log(result);

	if(result === null){
		return res.send("Email is available.")
	} else {
		return res.send("Email is already registered!")
	}
})
	.catch(err=> res.send(err))

}


module.exports.updateUserDetails=(req,res)=>{
	console.log(req.body)
	console.log(req.user.id)
	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}
	User.findByIdAndUpdate(req.user.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
}

module.exports.updateToAdmin=(req,res)=>{
	console.log(req.user)
	console.log(req.params.id)

	User.findByIdAndUpdate(req.params.id,{isAdmin:true},{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
}

module.exports.enroll = async (req,res)=>{
console.log(req.user.id)

	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

let isUserUpdated = await User.findById(req.user.id).then(user=>{
	console.log(user);

	let newEnrollment = {
		courseId: req.body.courseId
	}

	user.enrollments.push(newEnrollment);

	return user.save().then(user=> true).catch(err=>err.message)
})
	//console.log(isUserUpdated);

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course=>{
		//console.log(course);

		let enrollee = {
			userId: req.user.id
		}
		//push enrollee into the course
		course.enrollees.push(enrollee);

		return course.save().then(course=>true).catch(err=>err.message)
	})
	//console.log(isCourseUpdated);
	if(isCourseUpdated !== true) {
		return res.send({message: isCourseUpdated})
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}
}

module.exports.getEnrollments = (req,res)=>{
	
	User.findById(req.user.id)
	.then(user => res.send(user.enrollments))
	.catch(err => res.send(err));

	// User.findById(req.user.id)
	// .then(result => res.send(user.enrollments))
	// .catch(err => res.send(err));

}