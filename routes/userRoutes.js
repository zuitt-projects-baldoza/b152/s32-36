const express = require("express");
const router = express.Router();

//import user controllers
const userControllers = require("../controllers/userControllers");

//import auth module so we can use verify and verifyAdmin method as middleware for our routes.
const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// console.log(verify);

//User Registration:
router.post("/",userControllers.registerUser);

//Mini-Activity
router.get('/',userControllers.getAllUsersController);

router.post('/login',userControllers.loginUser);

router.get('/getUserDetails',verify,userControllers.getUserDetails);

router.post('/checkEmailExists',userControllers.checkEmailExists);

router.put("/updateUserDetails",verify,userControllers.updateUserDetails);

router.put("/updateToAdmin/:id",verify,verifyAdmin,userControllers.updateToAdmin);

router.post('/enroll',verify,userControllers.enroll);

router.post('/getEnrollments',verify,userControllers.getEnrollments);

module.exports = router;


