const express = require("express");
const router = express.Router();

//import course controllers
const courseControllers = require("../controllers/courseControllers");

//import auth
const auth = require("../auth");
const {verify,verifyAdmin} = auth; 

//Course Registration:
router.post("/",verify,verifyAdmin,courseControllers.registerCourse);

router.get('/',courseControllers.getAllCourseController);

router.get('/getSingleCourse/:id',courseControllers.getSingleCourse);

router.put("/:id",verify,verifyAdmin,courseControllers.updateCourse);

router.put("/archive/:id",verify,verifyAdmin,courseControllers.archiveCourse);

router.put("/activate/:id",verify,verifyAdmin,courseControllers.activateCourse);

router.get('/getAllActiveCourse',courseControllers.getAllActiveCourse);

router.get('/getInactiveCourse',courseControllers.getInactiveCourse);

router.post('/findCoursesByName/',courseControllers.findCoursesByName);

router.post('/findCoursesByPrice/',courseControllers.findCoursesByPrice);

router.post('/getEnrollees/:id',courseControllers.getEnrollees);

module.exports = router;

