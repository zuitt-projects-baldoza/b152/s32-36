const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://supeeerb:mongoDB@cluster0.tranx.mongodb.net/bookingAPI152?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//notifs for successful or failed mongodb connection:
let db = mongoose.connection;
db.on("error",console.error.bind(console,"Connection Error"));
db.once("open",()=>console.log("Connected to MongoDB"));

app.use(express.json());

//User
//import route from userRoutes
const userRoutes = require('./routes/userRoutes')
//use our routes and group together under '/users'
app.use('/users',userRoutes);

//Course
const courseRoutes = require('./routes/courseRoutes')
app.use('/course',courseRoutes);

app.listen(port,()=>console.log(`Server running at port ${port}`));